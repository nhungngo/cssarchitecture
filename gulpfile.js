var gulp = require('gulp');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');

gulp.task('sass', function() {
  return gulp
    .src('src/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./public'))
    .pipe(livereload());
});

// Watch Files For Changes
gulp.task('watch', function() {
  livereload.listen();
  gulp.watch(['src/*.scss'], gulp.series('sass'));
});
