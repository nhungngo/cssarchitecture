function activeTab(event, tabBodyId) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName('tab-body');
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = 'none';
  }
  tablinks = document.getElementsByClassName('tab-item');
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(' active', '');
  }
  document.getElementById(tabBodyId).style.display = 'block';
  event.currentTarget.className += ' active';
}
