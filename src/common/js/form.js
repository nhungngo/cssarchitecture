function setIndeterminate(element) {
  let checkboxIndeterminate = document.querySelector(element);
  checkboxIndeterminate.indeterminate = true;
}
