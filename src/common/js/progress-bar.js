function setState(element, state) {
  let pg, parentWidth, currentWidth, currentState;
  setInterval(() => {
    pg = document.querySelector(element);
    parentWidth = pg.parentElement.clientWidth;
    currentWidth = pg.clientWidth;
    currentState = (currentWidth / parentWidth) * 100;
    if (currentState < 100) {
      pg.style.width = `${currentState + state}%`;
    }
  }, 1000);
}
